(require 'expand-region)
(require 'comint)

(defun mosche-hook ()
  (local-set-key (kbd "C-c C-c") 'mosche-send-last-sexp)
  (local-set-key (kbd "C-c C-p") 'mosche-start-comint))

(add-hook 'scheme-mode-hook 'mosche-hook)

(defun mosche-start-comint ()
  "Run scheme command to start a comint"
  (interactive)
  (make-comint "scheme" "scheme")
  (switch-to-buffer-other-window "*scheme*"))


(defun mosche-send-last-sexp ()
  (interactive)
  (save-excursion
    (if (not (use-region-p))
        (er--expand-region-1))
    (mosche-send-region
     (region-beginning)
     (region-end)))
  (deactivate-mark))

(defun mosche-send-region (beg end)
  "Send region to comint buffer"
  (interactive "r")
  (let ((timeout 0)
        (caller-buffer (current-buffer)))
    (if (not (get-buffer "*scheme*"))
        (progn
          (mosche-start-comint)
          (while (not (eq (current-buffer) caller-buffer))
            (other-window 1)))))
  (comint-send-region "*scheme*"
                      beg
                      end))

(provide 'mosche)
